#!/bin/bash
cd /vagrant/script
/bin/bash unswap.sh
/bin/bash apt-upgrade.sh
/bin/bash common-packages.sh
/bin/bash dev-libs.sh
/bin/bash jenkins-install.sh
apt-get install -y nginx
rm /etc/nginx/sites-enabled/default
cp /vagrant/config/nginx-jenkins /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/nginx-jenkins /etc/nginx/sites-enabled/
service nginx restart
/bin/bash docker-install.sh
usermod -aG docker jenkins
usermod -aG docker ubuntu
