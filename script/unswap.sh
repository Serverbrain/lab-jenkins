#!/usr/bin/env bash

### info: reduce swappiness
### dist: ubuntu/xenial
### user: root

echo 'vm.swappiness=0' > /etc/sysctl.d/swappiness.conf
