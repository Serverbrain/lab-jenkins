#!/usr/bin/env bash

### info: Install Development Libraries
### dist: ubuntu/xenial
### user: root

export DEBIAN_FRONTEND=noninteractive
apt-get install -y \
build-essential \
libbz2-dev \
libcurl4-openssl-dev \
libffi-dev \
libfreetype6-dev \
libgmp-dev \
libjpeg-dev \
liblcms2-dev \
liblzma-dev \
libncurses5 \
libncurses5-dev \
libncursesw5 \
libopenjpeg-dev \
libpng12-dev \
libpq-dev \
libreadline-dev \
libsqlite3-dev \
libssl-dev \
libtiff5-dev \
libwebp-dev \
libxml2 \
libxml2-dev \
libxslt1-dev \
libxslt1.1 \
libyaml-dev \
zlib1g-dev
