#!/usr/bin/env bash

### info: install common packages
### dist: ubuntu/xenial
### user: root

export DEBIAN_FRONTEND=noninteractive
apt-get install -y \
apt-transport-https \
ca-certificates \
curl \
dnsutils \
git \
htop \
mc \
multitail \
ntp \
openssl \
python-software-properties \
rsync \
screen \
sqlite3 \
sudo \
ufw \
unzip \
vim \
wget \
zip
