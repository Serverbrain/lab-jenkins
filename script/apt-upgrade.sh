#!/usr/bin/env bash

### info: apt update and upgrade
### dist: ubuntu/xenial
### user: root

export DEBIAN_FRONTEND=noninteractive
apt-get update -y && apt-get dist-upgrade -y
