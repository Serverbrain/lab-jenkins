#!/usr/bin/env bash

### info: install docker
### dist: ubuntu/xenial
### user: root

DOCKER_COMPOSE_VERSION=1.8.0

APT_REPO_KEY="58118E89F3A912897C070ADBF76221572C52609D"
APT_REPO_FILE="/etc/apt/sources.list.d/docker.list"
APT_REPO_SOURCE="deb https://apt.dockerproject.org/repo ubuntu-xenial main"

export DEBIAN_FRONTEND=noninteractive

apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys ${APT_REPO_KEY} 

if [ ! -f "$APT_REPO_FILE" ]
then
  echo "$APT_REPO_SOURCE" > "$APT_REPO_FILE"
fi

apt-get update -y
apt-get upgrade -y
apt-get install -y docker-engine

curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m) > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
