#!/usr/bin/env bash

### info: install jenkins-2 from pkg.jenkins.io repo
### dist: ubuntu/xenial
### user: root

APT_REPO_KEY="https://pkg.jenkins.io/debian/jenkins-ci.org.key"
APT_REPO_FILE="/etc/apt/sources.list.d/jenkins-repo.list"
APT_REPO_SOURCE="deb http://pkg.jenkins.io/debian-stable binary/"

export DEBIAN_FRONTEND=noninteractive

if [ ! -f "$APT_REPO_FILE" ]
then
  echo "$APT_REPO_SOURCE" > "$APT_REPO_FILE"
  wget --quiet -O - "$APT_REPO_KEY" | apt-key add -
fi

apt-get update -y
apt-get upgrade -y
apt-get install -y jenkins
